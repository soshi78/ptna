# -*- coding: utf-8 -*-
"""
Created on Mon Jul  3 22:47:56 2017

@author: syo
"""
from responder import *


class Ptna:
    """ピティナの本体クラス
    """
    def __init__(self,name):
        """Ptnaオブジェクトの名前をnameに格納
           Responderオブジェクトを生成してresponderに
           @param name Ptnaオブジェクトの名前
        """
        self.name = name
        self.responder = Responder("What")
        
    def dialogue(self,input):
        """応答オブジェクトのresponder()を呼び出して
            応答文字列を作成する
            
            @params input ユーザによって入力された文字列
            戻り値 応答文字列
        """
        return self.responder.response(input)
    
    def get_responder_name(self):
        """応答オブジェクトの名前を返す
        """
        return self.responder.name
    
    def get_name(self):
        """Ptnaオブジェクトの名前を返す
        """
        return self.name